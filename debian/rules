#!/usr/bin/make -f

export DH_GOLANG_INSTALL_ALL := 1
export DH_GOLANG_EXCLUDES := hub\d+ data\d+

export BUILD_VERSION   := $(shell dpkg-parsechangelog -SVersion)
export BUILD_TAG       := debian
export BUILD_CODENAME  := $(shell awk '/CodeName/ { gsub(/["]/, "", $$2); print $$2 }' RELEASE.json)
export BUILD_GOVERSION := $(shell go version | awk '{ gsub(/^go/, "", $$3); print $$3 }')
export BUILD_DATE      := $(shell TZ=Etc/UTC date +'%F_%T' -d @$(SOURCE_DATE_EPOCH))
export set_cwversion   := -X github.com/crowdsecurity/crowdsec/pkg/cwversion
export LD_FLAGS        := -ldflags '-s -w             \
	$(set_cwversion).Version=$(BUILD_VERSION)     \
	$(set_cwversion).Tag=$(BUILD_TAG)             \
	$(set_cwversion).Codename=$(BUILD_CODENAME)   \
	$(set_cwversion).GoVersion=$(BUILD_GOVERSION) \
	$(set_cwversion).BuildDate=$(BUILD_DATE)      \
'

# Use 1 for a new upstream release, and bump it when an update of the
# hub files is desired while the upstream version doesn't change. See
# below for the generate_hub_tarball target:
export DATA_ID    := 1
export HUB_ID     := 1
export HUB_BRANCH := master
export HUB_DIR    := ../hub
export U_VERSION  := $(shell dpkg-parsechangelog -SVersion|sed 's/-.*//')

%:
	dh $@ --builddirectory=_build --buildsystem=golang --with=golang

override_dh_auto_build:
	dh_auto_build -- $(LD_FLAGS)

override_dh_auto_install:
	dh_auto_install --

override_dh_install:
	dh_install
	# Switch from Golang naming to upstream-desired naming:
	mv debian/crowdsec/usr/bin/crowdsec-cli \
		debian/crowdsec/usr/bin/cscli
	# Adjust the hub branch according to the upstream version:
	sed "s/\(.*hub_branch:\) master/\1 v$(U_VERSION)/" -i debian/crowdsec/etc/crowdsec/config.yaml
	# Drop unit tests from the hub:
	find debian/crowdsec/usr/share/crowdsec/hub -depth -name '.tests' -exec rm -rf '{}' ';'


### Maintainer targets:

generate_hub_tarball:
	cd $(HUB_DIR) && git archive --prefix hub$(HUB_ID)/ $(HUB_BRANCH) | gzip -9 > ../crowdsec_$(U_VERSION).orig-hub$(HUB_ID).tar.gz \
	&& echo "Generated hub tarball from branch $(HUB_BRANCH), at commit `git show $(HUB_BRANCH) | awk '/^commit / {print $$2; quit}' | cut -b -10`"

extract_hub_tarball:
	tar xf ../crowdsec_$(U_VERSION).orig-hub$(HUB_ID).tar.gz

extract_data_tarball:
	tar xf ../crowdsec_$(U_VERSION).orig-data$(HUB_ID).tar.gz
